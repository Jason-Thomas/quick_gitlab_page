var tabulate = function (data,columns) {
      var table = d3.select('div.table_container').append('table').attr('id', 'myTable')
      var thead = table.append('thead')
      var tbody = table.append('tbody')
  
      thead.append('tr')
        .selectAll('th')
          .data(columns)
          .enter()
        .append('th')
          .text(function (d) { return d })
  
      var rows = tbody.selectAll('tr')
          .data(data)
          .enter()
        .append('tr')

    var total = 0;
    var tcovered = 0;
    var Ecovered = 0;
    tbody.selectAll('tr')
          .filter(function(d,i) {            
            if (data[i]['Training Covered'].length > 0 && data[i]['Eval Covered'].length > 0) {
                this.style = 'background-color: #58e139';
                tcovered++;
                Ecovered++;
            }
            else if (data[i]['Training Covered'].length > 0) {
                this.style = 'background-color: #e7de12';
                tcovered++;
            }
            else if (data[i]['Eval Covered'].length > 0) {
                this.style = 'background-color: #e7de12';
                Ecovered++;
            }
            total++;
          })
    
          
    var gauge1 = loadLiquidFillGauge("fillgauge1", (tcovered/total)*100);
    var gauge2 = loadLiquidFillGauge("fillgauge2", (Ecovered/total)*100);

      var cells = rows.selectAll('td')
          .data(function(row) {
              return columns.map(function (column) {
                  return { column: column, value: row[column] }
            })
        })
        .enter()
      .append('td')
        .text(function (d) { return d.value })
  
    return table;
  }
  
  d3.json('MTTL.json',function (data) {
      var columns = ['UID','Req Src','Req Src ID','Requirement Owner(s)','Category','Topic/Language/Tool/Application','TASK','SUB TASK','Basic Dev Crit Task','Basic Dev','Sdev-Win Crit Task','Sdev-Win','Sdev-Lin Crit Task','Sdev-Lin','SEE Crit Task','SEE','Embedded Crit Task','Embedded','Network Crit Task','Network','Mobile Crit Task','Mobile','Basic PO Crit Task','Basic PO','Senior PO Crit Task','Senior PO','COMMENTS', 'Training Covered', 'Eval Covered']
    tabulate(data,columns)
  })
  