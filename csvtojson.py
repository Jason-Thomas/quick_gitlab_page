#!/usr/bin/python3

import json, csv, os

avoid = [
    'lesson_template.json',
    'tasks.json',
    'launch.json'
]

def transform():
    with open('home/MTTL.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        data = []
        for row in reader:
            KSA = {}
            for key in row.keys():
                if key is not "":
                    KSA[key] = row[key]
            KSA['Training Covered'] = ""
            KSA['Eval Covered'] = ""
            data.append(KSA)
        with open("home/MTTL.json", 'w') as jsonfile:
            json.dump(data, jsonfile, sort_keys=False, indent=4)

def append_mttl_ksa_trn(json_paths):
    with open("home/MTTL.json", "r+") as mttlfile:
        mttl = json.load(mttlfile)
        for path in json_paths:
            with open(path[1]) as trndata:
                trnjson = json.load(trndata)
                for ksa in trnjson["ksas"]:
                    mttlksa = mttl[ksa - 1]
                    if mttlksa['Training Covered'] is "":
                        mttlksa["Training Covered"] = trnjson["course"]
                    else:
                        mttlksa["Training Covered"] += f', {trnjson["course"]}'
        mttlfile.seek(0)
        json.dump(mttl, mttlfile, sort_keys=False, indent=4)

def append_mttl_ksa_evl(json_paths, ksa_measure):
    with open("home/MTTL.json", "r+") as mttlfile:
        mttl = json.load(mttlfile)
        for path in json_paths:
            with open(path[1]) as trndata:
                trnjson = json.load(trndata)
                for ksa in trnjson["ksas"]:
                    mttlksa = mttl[ksa - 1]
                    if mttlksa['Eval Covered'] is "":
                        mttlksa["Eval Covered"] = path[0]
                    else:
                        mttlksa["Eval Covered"] += f', {path[0]}'
        mttlfile.seek(0)
        json.dump(mttl, mttlfile, sort_keys=False, indent=4)

def find_jsons(input_path):
    data = []
    for dir_path, subdir_list, file_list in os.walk(input_path):
        for fname in file_list:
            if '.json' in fname and fname not in avoid:
                tmp = (fname, os.path.join(dir_path, fname))
                data.append(tmp)
    return data
        
transform()
append_mttl_ksa_trn(find_jsons('./idf-course'))
append_mttl_ksa_evl(find_jsons('./knowledge/questions'), 'Knowledge')
append_mttl_ksa_evl(find_jsons('./performance/Basic'), 'Performance')
append_mttl_ksa_evl(find_jsons('./performance/Senior'), 'Performance')
